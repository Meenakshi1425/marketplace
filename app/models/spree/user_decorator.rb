module Spree::UserDecorator
    def self.prepended(base)
        base.accepts_nested_attributes_for :vendors
    end
end

Spree.user_class.prepend Spree::UserDecorator