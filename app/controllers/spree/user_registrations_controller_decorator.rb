module Spree
	Spree::UserRegistrationsController.class_eval do
		def new
			super
			@user = Spree::User.new
			@user.vendors.build
		end

		def create
			@user = build_resource(spree_user_params)
			resource_saved = resource.save
			yield resource if block_given?
			if resource_saved
				if resource.active_for_authentication?
					set_flash_message :notice, :signed_up
					sign_up(resource_name, resource)
					session[:spree_user_signup] = true
					redirect_to_checkout_or_account_path(resource)
				else
					set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}"
					expire_data_after_sign_in!
					respond_with resource, location: after_inactive_sign_up_path_for(resource)
				end
			else
				clean_up_passwords(resource)
				render :new
			end
		end

		private
		def spree_user_params
			if params[:spree_user][:customer_type] == 'Vendor'
				params[:spree_user][:vendors_attributes] = Array.wrap(params[:spree_user][:vendors_attributes].merge({ :state => "active" }))
			else
				params[:spree_user].delete(:vendors_attributes)
			end
			params.require(:spree_user).permit(Spree::PermittedAttributes.user_attributes)
		end
	
		def redirect_to_checkout_or_account_path(resource)
			resource_path = after_sign_up_path_for(resource)
	
			if resource_path == spree.checkout_state_path(:address)
				respond_with resource, location: spree.checkout_state_path(:address)
			else
				if resource.customer_type == 'Vendor'
					respond_with resource, location: spree.admin_path
				else
					respond_with resource, location: spree.root_path
				end
			end
		end
	end
end