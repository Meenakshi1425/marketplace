class AddDefaultValueToUsers < ActiveRecord::Migration[5.1]
  def up
    change_column :spree_users, :customer_type, :string, :default => "Vendor"
  end

  def down
    change_column :spree_users, :customer_type, :string, :default => "User"
  end
end
